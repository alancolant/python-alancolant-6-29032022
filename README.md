# Installation

## Download

```sh
git clone --depth=1 https://gitlab.com/alancolant/python-alancolant-6-29032022.git folder
cd folder &&  git submodule update --init --recursive
```

## Backend

Execute related [Instructions](https://github.com/OpenClassrooms-Student-Center/OCMovies-API-EN-FR) in
the `sources/backend`folder

## Frontend

```shell
cd sources/frontend
#You can load index.html into you browser, but some CORS error may appear.
#If you have CORS errors run:
node start-server.js
#Follow the URL
```