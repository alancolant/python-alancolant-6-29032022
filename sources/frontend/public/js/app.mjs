import FilmSlider from "../custom-elements/FilmSlider.mjs";
import FilmModal from "../custom-elements/FilmModal.mjs";
import TopFilm from "../custom-elements/TopFilm.mjs";

FilmSlider.defineElement()
FilmModal.defineElement()
TopFilm.defineElement()