export const baseUrl = "http://127.0.0.1:8000/api/v1"

const useFetch = (path, params = null) => {
    let url = baseUrl + path
    if (params != null) url += '?' + new URLSearchParams(params).toString()
    return fetch(url).then(r => {
        if (!r.ok) throw new Error('Error fetching')
        return r.json()
    })
}

export const useMostRatedFilms = (page = 1, genre = null, paginate = 10) => {
    const params = {sort_by: ['-imdb_score', '-year'].join(','), page_size: paginate, page}
    if (genre !== null) params.genre = genre
    return useFetch('/titles/', params).then(r => r['results'])
}


export const useFilm = (filmId) => {
    return useFetch('/titles/' + filmId)
}
