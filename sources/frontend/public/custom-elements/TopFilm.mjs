import BaseElement from "./BaseElement.mjs";
import {useMostRatedFilms} from "../js/api.mjs";

export default class TopFilm extends BaseElement {
    static ce_name = 'top-film';

    async connectedCallback() {
        const topFilm = await useMostRatedFilms(1, null, 1).then(r => r[0])//Fetch top film
        this.render(topFilm)
    }

    render(film) {
        this.innerHTML = `
<img src="${film['image_url']}" alt="Image de ${film['title']}">
<div>
    <h1>${film['title']}</h1>
    <button>Play</button>
</div>`;
    }
}