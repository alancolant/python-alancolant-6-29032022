import BaseElement from "./BaseElement.mjs";
import {useFilm} from "../js/api.mjs";

export default class FilmModal extends BaseElement {
    static ce_name = 'film-modal';

    constructor() {
        super();

        this.film = null

        const modal = document.createElement('div')
        this.style.setProperty('opacity', '0')
        modal.classList.add('modal')
        this.modal = this.insertAdjacentElement('beforeend', modal)
        this.addEventListener('click', this.onClick)

    }

    render() {
        const film = this.film;
        // const hours = Math.floor(film['duration'] / 60)
        const hours = ~~(film['duration'] / 60)
        const minutes = String(film['duration'] % 60).padStart(2, '0')
        this.modal.innerHTML = `
        <figure>
            <img src="${film['image_url']}"
                 alt="Image du film ${film['title']}">
            <figcaption>
                <h1>${film['title']}</h1>
                <quote>Réalisateur: ${film['directors'].join(', ')}</quote>
                <p>Acteurs: ${film['actors'].join(', ')}</p>
                <p>Date de sortie: ${film['year']}</p>
                <p>Durée: ${hours}h${minutes}</p>
                <p>${film['genres'].map((f) => '<span class="badge">' + f + '</span>').join('\n')}</p>
            </figcaption>
        </figure>
        <div>
            <p class="lg">Résumé</p>
            <p>${film['long_description'] ?? film['description']}</p>

            <p>Evaluation: ${film['rated'] ?? 'Inconnu'}</p>
            <p>Score Imdb:  ${film['imdb_score'] ?? 'Inconnu'}</p>
            <p>Box-Office: ${film['worldwide_gross_income'] ?? 'Inconnu'}</p>
            <p>Pays d'origine:  ${film['countries'].join(', ') ?? 'Inconnu'}</p>
        </div>`
    }

    onClick(event) {
        if (event.target === this) this.remove()
    }

    async connectedCallback() {
        this.film = await useFilm(this.getAttribute('film-id'))
        //Disable bodyscroll
        this.style.setProperty('opacity', '1')
        document.querySelector('body').style.overflow = "hidden";
        this.render()
    }

    disconnectedCallback() {
        this.removeEventListener('click', this.onClick)
        //Enable body scroll
        document.querySelector('body').style.overflow = "auto";
    }
}