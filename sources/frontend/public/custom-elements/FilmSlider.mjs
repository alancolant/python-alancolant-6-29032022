import BaseElement from "./BaseElement.mjs";
import {useMostRatedFilms} from "../js/api.mjs";

export default class FilmSlider extends BaseElement {
    static ce_name = 'film-slider';

    constructor() {
        super();

        this.page = 1;
        this.maxPage = 10;
        this.lastFetchedPage = 0;

        const prevSpan = document.createElement('span')
        prevSpan.classList.add('prev')
        prevSpan.classList.add('disabled') //First page
        prevSpan.innerHTML = "<svg xmlns=\"http://www.w3.org/2000/svg\" class=\"h-6 w-6\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\" stroke-width=\"2\">\n" +
            "  <path stroke-linecap=\"round\" stroke-linejoin=\"round\" d=\"M15 19l-7-7 7-7\" />\n" +
            "</svg>"
        prevSpan.addEventListener('click', () => this.onPrevEvent());
        this.prevSpan = this.appendChild(prevSpan)

        const filmContainer = document.createElement('div')
        filmContainer.classList.add('films')
        this.filmContainer = this.appendChild(filmContainer)

        const nextSpan = document.createElement('span')
        nextSpan.classList.add('next')
        nextSpan.innerHTML = "<svg xmlns=\"http://www.w3.org/2000/svg\" class=\"h-6 w-6\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\" stroke-width=\"2\">\n" +
            "  <path stroke-linecap=\"round\" stroke-linejoin=\"round\" d=\"M9 5l7 7-7 7\" />\n" +
            "</svg>"
        nextSpan.addEventListener('click', () => this.onNextEvent());
        this.nextSpan = this.appendChild(nextSpan)
    }

    async connectedCallback() {
        const films = await useMostRatedFilms(
            this.page,
            this.getAttribute('genre'),
            parseInt(this.getAttribute('paginate')) * 2 //Load one slide more
        )
        films.forEach(f => this.appendFilm(f))
        this.lastFetchedPage = this.page + 1;
    }

    onPrevEvent() {
        if (this.page === 1) return
        this.page--;
        this.filmContainer.style.setProperty('left', '-' + (this.page - 1) * 100 + '%')
        this.setDisablingNavigation()
    }

    onNextEvent() {
        if (this.page === this.maxPage) return
        if (this.page > this.lastFetchedPage) return

        this.page++;
        this.filmContainer.style.setProperty('left', '-' + (this.page - 1) * 100 + '%')

        //Lazy load next page
        if (this.page > this.lastFetchedPage - 1) {
            useMostRatedFilms(
                this.page + 1,
                this.getAttribute('genre'),
                this.getAttribute('paginate')
            ).then(res => res.forEach(film => {
                this.lastFetchedPage = this.page + 1;
                this.appendFilm(film)
            }))
        }
        this.setDisablingNavigation()
    }

    appendFilm(film) {
        const figure = document.createElement('figure');
        const img = document.createElement('img');
        img.src = film['image_url']
        img.alt = "Image du film " + film.title
        figure.appendChild(img)
        //Add listener onClick on the film
        figure.addEventListener('click', () => this.showModal(film.id))
        this.filmContainer.append(figure)
    }

    setDisablingNavigation() {
        if (this.page === 1) {
            this.prevSpan.classList.add('disabled')
        } else if (this.page === this.maxPage) {
            this.nextSpan.classList.add('disabled')
        } else {
            this.prevSpan.classList.remove('disabled')
            this.nextSpan.classList.remove('disabled')
        }
    }

    showModal(filmId) {
        console.log(filmId)
        document.querySelector('body')
            .insertAdjacentHTML('afterbegin', `<film-modal film-id="${filmId}"></film-modal>`)
    }
}